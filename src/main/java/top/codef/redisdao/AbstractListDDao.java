package top.codef.redisdao;

import java.util.Collection;
import java.util.List;

import org.springframework.data.redis.core.ListOperations;

public abstract class AbstractListDDao extends AbstractRedisDDao {

	public ListOperations<String, String> opsForList() {
		return getStringRedisTemplate().opsForList();
	}

	public String leftPop(String key) {
		String value = opsForList().leftPop(getKey(key));
		return value;
	}

	public String rightPop(String key) {
		var value = opsForList().rightPop(getKey(key));
		return value;
	}

	public void rightPush(String key, String value) {
		opsForList().rightPush(getKey(key), value);
	}

	public void leftPush(String key, String value) {
		opsForList().leftPush(getKey(key), value);
	}

	public List<String> getAll(String key) {
		List<String> list = opsForList().range(getKey(key), 0, -1);
		return list;
	}

	public long pushAll(String key, Collection<String> list) {
		long count = opsForList().rightPushAll(getKey(key), list);
		return count;
	}

	public Long remove(String key, Long count, String value) {
		return opsForList().remove(getKey(key), 0, value);
	}

}
