package top.codef.dao;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaDelete;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.CriteriaUpdate;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import top.codef.exceptions.JpaAmebaException;
import top.codef.models.Page;
import top.codef.models.Pageable;
import top.codef.sqlfilter.CommonFilter;
import top.codef.sqlfilter.QueryBuilder;
import static top.codef.dao.DaoUtils.*;

public abstract class BaseDaoWithClass<T> extends AbstractDaoWithClass<T> {

	private final Log logger = LogFactory.getLog(getClass());

	/**
	 * 获取单个实例
	 * 
	 * @param clazz
	 * 
	 * @param commonFilter
	 * @return
	 */
	public T getSingle(CommonFilter commonFilter) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> query = builder.createQuery(clazz());
		Root<T> root = query.from(clazz());
		conditionHandle(commonFilter, query, builder, root);
		try {
			TypedQuery<T> typedQuery = getEntityManager().createQuery(query);
			limit(commonFilter, typedQuery);
			T result = typedQuery.getSingleResult();
			return result;
		} catch (NoResultException e) {
			logger.debug("无对象返回：" + clazz().getName());
		}
		return null;
	}

	/**
	 * 获取单个实例
	 * 
	 * @param <T>
	 * @param <K>
	 * @param tarClazz
	 * @param rootClass
	 * @param commonFilter
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <K> K getSingle(Class<K> tarClazz, CommonFilter commonFilter) {
		if (commonFilter.getSelectors().size() == 1) {
			Object result = getSingleWithSingleExistJavaObj(clazz(), commonFilter);
			return (K) result;
		}
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		if (commonFilter.getSelectors().size() == 0)
			throw new JpaAmebaException("没有选择的字段");
		CriteriaQuery<K> query = builder.createQuery(tarClazz);
		Root<T> root = query.from(clazz());
		conditionHandle(commonFilter, query, builder, root);
		try {
			TypedQuery<K> typedQuery = getEntityManager().createQuery(query);
			limit(commonFilter, typedQuery);
			K result = typedQuery.getSingleResult();
			return result;
		} catch (NoResultException e) {
			logger.warn("无对象返回：" + clazz().getName());
		}
		return null;
	}

	private <K> Object getSingleWithSingleExistJavaObj(Class<K> clazz, CommonFilter commonFilter) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Object> query = builder.createQuery();
		Root<K> root = query.from(clazz);
		conditionHandle(commonFilter, query, builder, root);
		try {
			TypedQuery<Object> typedQuery = getEntityManager().createQuery(query);
			limit(commonFilter, typedQuery);
			Object result = typedQuery.getSingleResult();
			return result;
		} catch (NoResultException e) {
			logger.warn("无对象返回：" + commonFilter.getSelectors() + "；对象：" + clazz);
		}
		return null;
	}

	public <K> List<K> getList(Class<K> tarClazz, CommonFilter commonFilter) {
		Class<T> rootClazz = clazz();
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<K> query = builder.createQuery(tarClazz);
		Root<T> root = query.from(rootClazz);
		conditionHandle(commonFilter, query, builder, root);
		TypedQuery<K> typedQuery = getEntityManager().createQuery(query);
		limit(commonFilter, typedQuery);
		List<K> re = typedQuery.getResultList();
		return re;
	}

//	public <T> List<T> mayGetList(Class<T> tarClazz, CommonFilter commonFilter) {
//		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
//		CriteriaQuery<T> query = builder.createQuery(tarClazz);
//		Root<?> root = query.from(rootClazz);
//		conditionHandle(commonFilter, query, builder, root);
//		TypedQuery<T> typedQuery = getEntityManager().createQuery(query);
//		limit(commonFilter, typedQuery);
//		List<T> re = typedQuery.getResultList();
//		return re;
//	}

	/**
	 * 分页所做查询列表
	 *
	 * @param <T>
	 * @param clazz
	 * @param page
	 * @param commonFilter
	 * @return
	 */
	public List<T> getList(Pageable page, CommonFilter commonFilter) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> query = builder.createQuery(clazz());
		Root<T> root = query.from(clazz());
		conditionHandle(commonFilter, page, query, builder, root);
		int first = page.getEachPageSize() * (page.getPageNo() - 1);
		List<T> list = getEntityManager().createQuery(query).setFirstResult(first).setMaxResults(page.getEachPageSize())
				.getResultList();
		return list;
	}

	/**
	 * @param tarClazz
	 * @param rootClazz
	 * @param page
	 * @param commonFilter
	 * @return
	 */
	public <R> List<R> getList(Class<R> tarClazz, Pageable page, CommonFilter commonFilter) {
		var rootClazz = clazz();
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<R> query = builder.createQuery(tarClazz);
		Root<T> root = query.from(rootClazz);
		conditionHandle(commonFilter, page, query, builder, root);
		int first = page.getEachPageSize() * (page.getPageNo() - 1);
		List<R> list = getEntityManager().createQuery(query).setFirstResult(first).setMaxResults(page.getEachPageSize())
				.getResultList();
		return list;
	}

	public Page<T> getPage(Pageable pageable, CommonFilter filter) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> query = builder.createQuery(clazz());
		Root<T> root = query.from(clazz());
		conditionHandle(filter, pageable, query, builder, root);
		Page<T> page = new Page<>(pageable);
		int first = pageable.getEachPageSize() * (pageable.getPageNo() - 1);
		List<T> list = getEntityManager().createQuery(query).setFirstResult(first)
				.setMaxResults(pageable.getEachPageSize()).getResultList();
		page.setContent(list);
		confirmPageCount(clazz(), page, builder, filter.getCountField(), filter);
		return page;
	}

	public <R> Page<R> getPage(Class<R> tarClazz, Pageable pageable, CommonFilter filter) {
		var rootClazz = clazz();
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<R> query = builder.createQuery(tarClazz);
		Root<T> root = query.from(rootClazz);
		conditionHandle(filter, pageable, query, builder, root);
		int first = pageable.getEachPageSize() * (pageable.getPageNo() - 1);
		List<R> list = getEntityManager().createQuery(query).setFirstResult(first)
				.setMaxResults(pageable.getEachPageSize()).getResultList();
		Page<R> page = new Page<>(pageable);
		page.setContent(list);
		confirmPageCount(rootClazz, page, builder, filter.getCountField(), filter);
		return page;

	}

	private void confirmPageCount(Class<?> clazz, Page<?> page, CriteriaBuilder builder, String field,
			CommonFilter filter) {
		Pageable pageable = page.getPageable();
		CriteriaQuery<Long> countQuery = builder.createQuery(Long.class);
		Root<?> path = countQuery.from(clazz);
		countConditionHandle(filter, countQuery, builder, path);
		Long count = getEntityManager().createQuery(countQuery).getSingleResult();
		pageable.setTotalCount(count);
		pageable.setPageCount((long) Math.ceil(pageable.getTotalCount().doubleValue() / pageable.getEachPageSize()));
	}

	/**
	 * 获取所有数据
	 * 
	 * @param clazz
	 * @return
	 */
	public List<T> getList() {
		return getList(clazz(), QueryBuilder.createFilter());
	}

	/**
	 * 获取列表
	 * 
	 * @param <T>
	 * @param clazz
	 * @param commonFilter
	 * @return
	 */
	public List<T> getList(CommonFilter commonFilter) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> query = builder.createQuery(clazz());
		Root<T> root = query.from(clazz());
		conditionHandle(commonFilter, query, builder, root);
		TypedQuery<T> typedQuery = getEntityManager().createQuery(query);
		limit(commonFilter, typedQuery);
		List<T> re = typedQuery.getResultList();
		return re;
	}

	/**
	 * 数量
	 *
	 * @param <T>
	 * @param clazz
	 * @param commonFilter
	 * @return
	 */
	public Long count(CommonFilter commonFilter) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> query = builder.createQuery(Long.class);
		Root<T> root = query.from(clazz());
		countConditionHandle(commonFilter, query, builder, root);
		Long count = getEntityManager().createQuery(query).getSingleResult();
		return count;
	}

	/**
	 * 数量
	 *
	 * @param <T>
	 * @param clazz
	 * @param field
	 * @param commonFilter
	 * @return
	 */
	public Long countDistinct(String field, CommonFilter commonFilter) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> query = builder.createQuery(Long.class);
		Root<T> root = query.from(clazz());
		if (commonFilter.getJoinList().size() > 0)
			joinTable(query, builder, commonFilter.getJoinList(), root);
		Predicate[] predicates = seperate(commonFilter, builder, root, query);
		query = predicates.length > 0
				? query.select(builder.countDistinct(root.get(field))).where(builder.and(predicates))
				: query.select(builder.countDistinct(root.get(field)));
		Long count = getEntityManager().createQuery(query).getSingleResult();
		return count;
	}

	public Integer update(CommonFilter commonFilter) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaUpdate<T> update = builder.createCriteriaUpdate(clazz());
		Root<T> root = update.from(clazz());
		commonFilter.getUpdatableList().stream().filter(x -> x.getValue() != null)
				.forEach(x -> update.set(root.get(x.getField()), x.getValue()));
		Predicate[] predicates = seperate(commonFilter, builder, root, update);
		update.where(predicates);
		int count = getEntityManager().createQuery(update).executeUpdate();
		return count;
	}

	public Integer updateWithNull(CommonFilter commonFilter) {
		var clazz = clazz();
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaUpdate<T> update = builder.createCriteriaUpdate(clazz);
		Root<T> root = update.from(clazz);
		commonFilter.getUpdatableList().stream().forEach(x -> update.set(root.get(x.getField()), x.getValue()));
		Predicate[] predicates = seperate(commonFilter, builder, root, update);
		update.where(predicates);
		int count = getEntityManager().createQuery(update).executeUpdate();
		return count;
	}

	public int delete(CommonFilter commonFilter) {
		Class<T> clazz = clazz();
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaDelete<T> delete = builder.createCriteriaDelete(clazz);
		Root<T> root = delete.from(clazz);
		Predicate[] predicates = seperate(commonFilter, builder, root, delete);
		if (predicates.length == 0)
			throw new JpaAmebaException("不支持的操作");
		delete.where(builder.and(predicates));
		int count = getEntityManager().createQuery(delete).executeUpdate();
		return count;
	}

	public int updateByHql(String hql, Map<String, Object> paramMap) {
		var query = getEntityManager().createQuery(hql);
		paramMap.forEach((x, y) -> query.setParameter(x, y));
		return query.executeUpdate();
	}

	public List<T> getListByHql(String hql, Map<String, Object> paramMap) {
		Class<T> clazz = clazz();
		var query = getEntityManager().createQuery(hql, clazz);
		paramMap.forEach((x, y) -> query.setParameter(x, y));
		return query.getResultList();
	}

	public T getSingleByHql(String hql, Map<String, Object> paramMap) {
		Class<T> clazz = clazz();
		var query = getEntityManager().createQuery(hql, clazz);
		paramMap.forEach((x, y) -> query.setParameter(x, y));
		return query.getSingleResult();
	}
}
