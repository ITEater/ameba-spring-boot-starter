package top.codef.dao;

import jakarta.persistence.EntityManager;

public interface AmebaDao {

	EntityManager getEntityManager();

	/**
	 * mysql可直接使用，生成数据库级唯一id方法;
	 * 
	 * @return 唯一id
	 * 
	 */
	default Long generateUid() {
		String sql = "select uuid_short();";
		return (Long) getEntityManager().createNativeQuery(sql).getSingleResult();
	}
//
//	default <T> CriteriaQuery<T> select(CommonFilter commonFilter, CriteriaQuery<T> query,
//			CriteriaBuilder criteriaBuilder, Path<?> path) {
//		List<SelectCondition> element = commonFilter.getSelectors();
//		if (!element.isEmpty()) {
//			Expression<?>[] expressions = element.stream().map(x -> x.select(criteriaBuilder, path))
//					.toArray(Expression<?>[]::new);
//			query.multiselect(expressions);
//		}
//		return query;
//	}
//
//	default <T> CriteriaQuery<Long> countSelect(CommonFilter commonFilter, CriteriaQuery<Long> query,
//			CriteriaBuilder criteriaBuilder, Path<T> path) {
//		if (commonFilter.getGroupingBy().size() > 0)
//			query.select(criteriaBuilder.countDistinct(commonFilter.getGroupingBy().get(0).groupBy(path)));
//		else if (commonFilter.getCountFeild() != null)
//			query.select(criteriaBuilder.countDistinct(PathUtils.getPath(commonFilter.getCountFeild(), path)));
//		else
//			query.select(criteriaBuilder.countDistinct(path));
//		return query;
//	}
//
//	default <T, R> CriteriaQuery<T> where(CommonFilter commonFilter, CriteriaQuery<T> query, CriteriaBuilder builder,
//			Root<R> root) {
//		if (commonFilter.getList().size() > 0) {
//			var predicates = seperate(commonFilter, builder, root);
//			query.where(builder.and(predicates));
//		}
//		return query;
//
//	}
//
//	default Predicate[] seperate(CommonFilter commonFilter, CriteriaBuilder builder, Root<?> root) {
//		Predicate[] array = commonFilter.getList().stream().map(x -> x.condition(builder, root))
//				.toArray(Predicate[]::new);
//		return array;
//	}
//
//	default <T, R> CriteriaQuery<T> groupBy(CommonFilter commonFilter, CriteriaQuery<T> query, CriteriaBuilder builder,
//			Root<R> root) {
//		var groupBy = commonFilter.getGroupingBy();
//		if (groupBy.size() > 0) {
//			Path<?>[] array = groupBy(commonFilter.getGroupingBy(), builder, root);
//			if (array != null)
//				query.groupBy(array);
//		}
//		return query;
//	}
//
//	default Path<?>[] groupBy(List<GroupCondition> groupConditions, CriteriaBuilder builder, Root<?> root) {
//		var grouping = groupConditions.stream().map(x -> x.groupBy(root)).toArray(Path<?>[]::new);
//		return grouping;
//	}
//
//	default void limit(CommonFilter commonFilter, Query query) {
//		if (commonFilter.hasLimit()) {
//			query.setMaxResults(commonFilter.getLimitCount());
//			var start = commonFilter.getLimitStart();
//			if (start != null) {
//				query.setFirstResult(start);
//			}
//		}
//	}
//
//	default List<Order> orderBy(List<OrderCondition> orders, Pageable page, CriteriaBuilder builder, Root<?> root) {
//		var orderList = orderBy(orders, builder, root);
//		if (page.getOrderStr() != null) {
//			Order order = page.getOrder() == OrderEnum.ASC ? builder.asc(root.get(page.getOrderStr()))
//					: builder.desc(root.get(page.getOrderStr()));
//			orderList.add(0, order);
//		}
//		return orderList;
//	}
//
//	default List<Order> orderBy(List<OrderCondition> orders, CriteriaBuilder builder, Root<?> root) {
//		List<Order> list = orders.stream().map(x -> x.order(builder, root)).collect(toList());
//		return list;
//	}
//
//	default <T> CriteriaQuery<T> orderBy(CommonFilter commonFilter, CriteriaQuery<T> query, CriteriaBuilder builder,
//			Root<?> root) {
//		var orderList = commonFilter.getOrderList();
//		if (orderList.size() > 0) {
//			query.orderBy(orderBy(orderList, builder, root));
//		}
//		return query;
//	}
//
//	default <T> CriteriaQuery<T> orderBy(CommonFilter commonFilter, CriteriaQuery<T> query, Pageable pageable,
//			CriteriaBuilder builder, Root<?> root) {
//		var orderList = commonFilter.getOrderList();
//		query.orderBy(orderBy(orderList, pageable, builder, root));
//		return query;
//	}
//
//	default void joinTable(List<JoinCondition> list, Root<?> root) {
//		if (list.size() > 0)
//			list.forEach(x -> x.join(root));
//	}
//
//	default <T, R> void conditionHandle(CommonFilter filter, CriteriaQuery<T> query, CriteriaBuilder builder,
//			Root<R> root) {
//		joinTable(filter.getJoinList(), root);
//		select(filter, query, builder, root);
//		where(filter, query, builder, root);
//		orderBy(filter, query, builder, root);
//		groupBy(filter, query, builder, root);
//	}
//
//	
//	
//	default <T, R> void conditionHandle(CommonFilter filter, Pageable pageable, CriteriaQuery<T> query,
//			CriteriaBuilder builder, Root<R> root) {
//		joinTable(filter.getJoinList(), root);
//		select(filter, query, builder, root);
//		where(filter, query, builder, root);
//		orderBy(filter, query, pageable, builder, root);
//		groupBy(filter, query, builder, root);
//	}
//
//	default <R> void countConditionHandle(CommonFilter filter, CriteriaQuery<Long> query, CriteriaBuilder builder,
//			Root<R> root) {
//		joinTable(filter.getJoinList(), root);
//		countSelect(filter, query, builder, root);
//		where(filter, query, builder, root);
//	}

}
