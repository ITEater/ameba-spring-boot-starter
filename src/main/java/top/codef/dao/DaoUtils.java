package top.codef.dao;

import static java.util.stream.Collectors.toList;

import java.util.List;

import jakarta.persistence.Query;
import jakarta.persistence.criteria.AbstractQuery;
import jakarta.persistence.criteria.CommonAbstractCriteria;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Order;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.Subquery;
import top.codef.enums.OrderEnum;
import top.codef.exceptions.JpaAmebaException;
import top.codef.models.Pageable;
import top.codef.sqlfilter.AbstractFilter;
import top.codef.sqlfilter.CommonFilter;
import top.codef.sqlfilter.HavingCommonFilter;
import top.codef.sqlfilter.JoinCommonFilter;
import top.codef.sqlfilter.PathUtils;
import top.codef.sqlfilter.SubCommonFilter;
import top.codef.sqlfilter.enums.SubSymbol;
import top.codef.sqlfilter.functional.GroupCondition;
import top.codef.sqlfilter.functional.JoinCondition;
import top.codef.sqlfilter.functional.OrderCondition;
import top.codef.sqlfilter.functional.SelectCondition;

public final class DaoUtils {

	public static <T, R> CriteriaQuery<T> where(AbstractFilter<?> commonFilter, CriteriaQuery<T> query,
			CriteriaBuilder builder, Root<R> root) {
		if (commonFilter.getList().size() > 0) {
			var predicates = seperate(commonFilter, builder, root, query);
			query.where(builder.and(predicates));
		}
		return query;
	}

	public static <T, R> Subquery<T> where(AbstractFilter<?> commonFilter, Subquery<T> query, CriteriaBuilder builder,
			Root<R> subRoot) {
		if (commonFilter.getList().size() > 0) {
			var predicates = seperate(commonFilter, builder, subRoot, query);
			query.where(builder.and(predicates));
		}
		return query;
	}

	public static Join<?, ?> on(JoinCommonFilter commonFilter, Root<?> root, CriteriaBuilder builder,
			CommonAbstractCriteria criteria, JoinType joinType) {
		return root.join(commonFilter.getJoinField(), joinType).on(builder.and(seperate(commonFilter, builder,
				commonFilter.isJoinObjectOnly() ? root.get(commonFilter.getJoinField()) : root, criteria)));
	}

	public static <T, R> CriteriaQuery<T> having(HavingCommonFilter filter, Root<R> root, CriteriaBuilder builder,
			CriteriaQuery<T> query) {
		if (filter == null)
			return query;
		var predicates = seperate(filter, builder, root, query);
		if (predicates.length > 0)
			query.having(predicates);
		return query;
	}

	public static <T, R> Subquery<T> having(HavingCommonFilter filter, Root<R> root, CriteriaBuilder builder,
			Subquery<T> query) {
		return filter == null ? query : query.having(seperate(filter, builder, root, query));
	}

	public static Predicate[] seperate(AbstractFilter<?> commonFilter, CriteriaBuilder builder, Path<?> root,
			CommonAbstractCriteria query) {
		Predicate[] array = commonFilter.getList().stream().map(x -> x.condition(query, builder, root))
				.toArray(Predicate[]::new);
		return array;
	}

//	public static Predicate[] seperate(CommonFilter commonFilter, CriteriaBuilder builder, Root<?> root,
//			Subquery<?> query) {
//		Predicate[] array = commonFilter.getList().stream().map(x -> x.condition(query, builder, root))
//				.toArray(Predicate[]::new);
//		return array;
//	}

//	public static <T> Predicate[] seperate(CommonFilter commonFilter, CriteriaBuilder builder, Root<T> root,
//			CriteriaDelete<T> delete) {
//		Predicate[] array = commonFilter.getList().stream().map(x -> x.condition(delete, builder, root))
//				.toArray(Predicate[]::new);
//		return array;
//	}
//
//	public static <T> Predicate[] seperate(CommonFilter commonFilter, CriteriaBuilder builder, Root<T> root,
//			CriteriaUpdate<T> update) {
//		Predicate[] array = commonFilter.getList().stream().map(x -> x.condition(update, builder, root))
//				.toArray(Predicate[]::new);
//		return array;
//	}

	public static <T, R> CriteriaQuery<T> groupBy(AbstractFilter<?> commonFilter, CriteriaQuery<T> query,
			CriteriaBuilder builder, Root<R> root) {
		var groupBy = commonFilter.getGroupingBy();
		if (groupBy.size() > 0) {
			Expression<?>[] array = groupBy(commonFilter.getGroupingBy(), builder, root);
			if (array != null)
				query.groupBy(array);
		}
		return query;
	}

	public static <T, R> AbstractQuery<T> groupBy(AbstractFilter<?> commonFilter, Subquery<T> query,
			CriteriaBuilder builder, Root<R> root) {
		var groupBy = commonFilter.getGroupingBy();
		if (groupBy.size() > 0) {
			Expression<?>[] array = groupBy(commonFilter.getGroupingBy(), builder, root);
			if (array != null)
				query.groupBy(array);
		}
		return query;
	}

	public static Expression<?>[] groupBy(List<GroupCondition> groupConditions, CriteriaBuilder builder, Root<?> root) {
		var grouping = groupConditions.stream().map(x -> x.groupBy(root, builder)).toArray(Expression<?>[]::new);
		return grouping;
	}

	public static void limit(CommonFilter commonFilter, Query query) {
		if (commonFilter.hasLimit()) {
			query.setMaxResults(commonFilter.getLimitCount());
			var start = commonFilter.getLimitStart();
			if (start != null) {
				query.setFirstResult(start);
			}
		}
	}

	public static List<Order> orderBy(List<OrderCondition> orders, Pageable page, CriteriaBuilder builder,
			Root<?> root) {
		var orderList = orderBy(orders, builder, root);
		if (page.getOrderStr() != null) {
			Order order = page.getOrder() == OrderEnum.ASC ? builder.asc(PathUtils.getPath(page.getOrderStr(), root))
					: builder.desc(PathUtils.getPath(page.getOrderStr(), root));
			orderList.add(0, order);
		}
		return orderList;
	}

	public static List<Order> orderBy(List<OrderCondition> orders, CriteriaBuilder builder, Root<?> root) {
		List<Order> list = orders.stream().map(x -> x.order(builder, root)).collect(toList());
		return list;
	}

	public static <T> CriteriaQuery<T> orderBy(AbstractFilter<?> commonFilter, CriteriaQuery<T> query,
			CriteriaBuilder builder, Root<?> root) {
		var orderList = commonFilter.getOrderList();
		if (orderList.size() > 0) {
			query.orderBy(orderBy(orderList, builder, root));
		}
		return query;
	}

	public static <T> CriteriaQuery<T> orderBy(AbstractFilter<?> commonFilter, CriteriaQuery<T> query,
			Pageable pageable, CriteriaBuilder builder, Root<?> root) {
		var orderList = commonFilter.getOrderList();
		query.orderBy(orderBy(orderList, pageable, builder, root));
		return query;
	}

	public static void joinTable(CommonAbstractCriteria criteria, CriteriaBuilder builder, List<JoinCondition> list,
			Root<?> root) {
		if (list.size() > 0)
			list.forEach(x -> x.join(criteria, root, builder));
	}

	public static <T, R> void conditionHandle(AbstractFilter<?> filter, CriteriaQuery<T> query, CriteriaBuilder builder,
			Root<R> root) {
		joinTable(query, builder, filter.getJoinList(), root);
		select(filter, query, builder, root);
		where(filter, query, builder, root);
		orderBy(filter, query, builder, root);
		groupBy(filter, query, builder, root);
		having(filter.getHavingCommonFilter(), root, builder, query);
	}

	public static <T, R> void conditionHandle(AbstractFilter<?> filter, Subquery<T> query, CriteriaBuilder builder,
			Root<R> root) {
		joinTable(query, builder, filter.getJoinList(), root);
		select(filter, query, builder, root);
		where(filter, query, builder, root);
		groupBy(filter, query, builder, root);
		having(filter.getHavingCommonFilter(), root, builder, query);
	}

	public static <T, R> void conditionHandle(AbstractFilter<?> filter, Pageable pageable, CriteriaQuery<T> query,
			CriteriaBuilder builder, Root<R> root) {
		joinTable(query, builder, filter.getJoinList(), root);
		select(filter, query, builder, root);
		where(filter, query, builder, root);
		orderBy(filter, query, pageable, builder, root);
		groupBy(filter, query, builder, root);
		having(filter.getHavingCommonFilter(), root, builder, query);
	}

	public static <R> void countConditionHandle(AbstractFilter<?> filter, CriteriaQuery<Long> query,
			CriteriaBuilder builder, Root<R> root) {
		joinTable(query, builder, filter.getJoinList(), root);
		countSelect(filter, query, builder, root);
		where(filter, query, builder, root);
		having(filter.getHavingCommonFilter(), root, builder, query);
	}

	public static <T> CriteriaQuery<T> select(AbstractFilter<?> commonFilter, CriteriaQuery<T> query,
			CriteriaBuilder criteriaBuilder, Path<?> path) {
		List<SelectCondition> element = commonFilter.getSelectors();
		if (!element.isEmpty()) {
			Expression<?>[] expressions = element.stream().map(x -> x.select(criteriaBuilder, path))
					.toArray(Expression<?>[]::new);
			query.multiselect(expressions);
		}
		return query;
	}

	@SuppressWarnings("unchecked")
	public static <T> Subquery<T> select(AbstractFilter<?> commonFilter, Subquery<T> query,
			CriteriaBuilder criteriaBuilder, Path<?> path) {
		List<SelectCondition> element = commonFilter.getSelectors();

		if (!element.isEmpty()) {
			Expression<T> expression = (Expression<T>) element.stream().map(x -> x.select(criteriaBuilder, path))
					.findFirst().orElse(null);
			if (expression == null)
				throw new JpaAmebaException("No expression can be used");
			query.select(expression);
		}
		return query;
	}

	public static <T> CriteriaQuery<Long> countSelect(AbstractFilter<?> commonFilter, CriteriaQuery<Long> query,
			CriteriaBuilder criteriaBuilder, Path<T> path) {
		if (commonFilter.getGroupingBy().size() > 0)
			query.select(
					criteriaBuilder.countDistinct(commonFilter.getGroupingBy().get(0).groupBy(path, criteriaBuilder)));
		else if (commonFilter.getCountField() != null)
			query.select(criteriaBuilder.countDistinct(PathUtils.getPath(commonFilter.getCountField(), path)));
		else
			query.select(criteriaBuilder.countDistinct(path));
		return query;
	}

	public static <R, T> Expression<T> subCondition(CommonAbstractCriteria criteria, CriteriaBuilder builder,
			SubCommonFilter<R, T> filter) {
		Subquery<T> subQ = criteria.subquery(filter.getTargetClazz());
		Root<R> subRoot = subQ.from(filter.getRootClazz());
		conditionHandle(filter, subQ, builder, subRoot);
		return genSub(builder, subQ, filter.getSubSymbol());
	}

	public static <T> Expression<T> genSub(CriteriaBuilder builder, Subquery<T> subquery, SubSymbol subSymbol) {
		switch (subSymbol) {
		case ALL:
			return builder.all(subquery);
		case ANY:
			return builder.any(subquery);
		default:
			throw new JpaAmebaException("no subSymbol");
		}
	}
}
