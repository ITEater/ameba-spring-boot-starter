package top.codef.dao;

import static java.util.stream.Collectors.toList;

import java.util.List;

import jakarta.persistence.Query;
import jakarta.persistence.criteria.AbstractQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaDelete;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.CriteriaUpdate;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Order;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.Subquery;
import top.codef.enums.OrderEnum;
import top.codef.exceptions.JpaAmebaException;
import top.codef.models.Pageable;
import top.codef.sqlfilter.CommonFilter;
import top.codef.sqlfilter.PathUtils;
import top.codef.sqlfilter.functional.GroupCondition;
import top.codef.sqlfilter.functional.JoinCondition;
import top.codef.sqlfilter.functional.OrderCondition;
import top.codef.sqlfilter.functional.SelectCondition;

public final class DaoUtils {

	public static <T, R> CriteriaQuery<T> where(CommonFilter commonFilter, CriteriaQuery<T> query,
			CriteriaBuilder builder, Root<R> root) {
		if (commonFilter.getList().size() > 0) {
			var predicates = seperate(commonFilter, builder, root, query);
			query.where(builder.and(predicates));
		}
		return query;
	}

	public static <T, R> Subquery<T> where(CommonFilter commonFilter, Subquery<T> query, CriteriaBuilder builder,
			Root<R> subRoot) {
		if (commonFilter.getList().size() > 0) {
			var predicates = seperate(commonFilter, builder, subRoot, query);
			query.where(builder.and(predicates));
		}
		return query;
	}

	public static Predicate[] seperate(CommonFilter commonFilter, CriteriaBuilder builder, Root<?> root,
			CriteriaQuery<?> query) {
		Predicate[] array = commonFilter.getList().stream().map(x -> x.condition(query, builder, root))
				.toArray(Predicate[]::new);
		return array;
	}

	public static Predicate[] seperate(CommonFilter commonFilter, CriteriaBuilder builder, Root<?> root,
			Subquery<?> query) {
		Predicate[] array = commonFilter.getList().stream().map(x -> x.condition(query, builder, root))
				.toArray(Predicate[]::new);
		return array;
	}

	public static <T> Predicate[] seperate(CommonFilter commonFilter, CriteriaBuilder builder, Root<T> root,
			CriteriaDelete<T> delete) {
		Predicate[] array = commonFilter.getList().stream().map(x -> x.condition(delete, builder, root))
				.toArray(Predicate[]::new);
		return array;
	}

	public static <T> Predicate[] seperate(CommonFilter commonFilter, CriteriaBuilder builder, Root<T> root,
			CriteriaUpdate<T> update) {
		Predicate[] array = commonFilter.getList().stream().map(x -> x.condition(update, builder, root))
				.toArray(Predicate[]::new);
		return array;
	}

	public static <T, R> CriteriaQuery<T> groupBy(CommonFilter commonFilter, CriteriaQuery<T> query,
			CriteriaBuilder builder, Root<R> root) {
		var groupBy = commonFilter.getGroupingBy();
		if (groupBy.size() > 0) {
			Path<?>[] array = groupBy(commonFilter.getGroupingBy(), builder, root);
			if (array != null)
				query.groupBy(array);
		}
		return query;
	}

	public static <T, R> AbstractQuery<T> groupBy(CommonFilter commonFilter, Subquery<T> query, CriteriaBuilder builder,
			Root<R> root) {
		var groupBy = commonFilter.getGroupingBy();
		if (groupBy.size() > 0) {
			Path<?>[] array = groupBy(commonFilter.getGroupingBy(), builder, root);
			if (array != null)
				query.groupBy(array);
		}
		return query;
	}

	public static Path<?>[] groupBy(List<GroupCondition> groupConditions, CriteriaBuilder builder, Root<?> root) {
		var grouping = groupConditions.stream().map(x -> x.groupBy(root)).toArray(Path<?>[]::new);
		return grouping;
	}

	public static void limit(CommonFilter commonFilter, Query query) {
		if (commonFilter.hasLimit()) {
			query.setMaxResults(commonFilter.getLimitCount());
			var start = commonFilter.getLimitStart();
			if (start != null) {
				query.setFirstResult(start);
			}
		}
	}

	public static List<Order> orderBy(List<OrderCondition> orders, Pageable page, CriteriaBuilder builder,
			Root<?> root) {
		var orderList = orderBy(orders, builder, root);
		if (page.getOrderStr() != null) {
			Order order = page.getOrder() == OrderEnum.ASC ? builder.asc(root.get(page.getOrderStr()))
					: builder.desc(root.get(page.getOrderStr()));
			orderList.add(0, order);
		}
		return orderList;
	}

	public static List<Order> orderBy(List<OrderCondition> orders, CriteriaBuilder builder, Root<?> root) {
		List<Order> list = orders.stream().map(x -> x.order(builder, root)).collect(toList());
		return list;
	}

	public static <T> CriteriaQuery<T> orderBy(CommonFilter commonFilter, CriteriaQuery<T> query,
			CriteriaBuilder builder, Root<?> root) {
		var orderList = commonFilter.getOrderList();
		if (orderList.size() > 0) {
			query.orderBy(orderBy(orderList, builder, root));
		}
		return query;
	}

	public static <T> CriteriaQuery<T> orderBy(CommonFilter commonFilter, CriteriaQuery<T> query, Pageable pageable,
			CriteriaBuilder builder, Root<?> root) {
		var orderList = commonFilter.getOrderList();
		query.orderBy(orderBy(orderList, pageable, builder, root));
		return query;
	}

	public static void joinTable(List<JoinCondition> list, Root<?> root) {
		if (list.size() > 0)
			list.forEach(x -> x.join(root));
	}

	public static <T, R> void conditionHandle(CommonFilter filter, CriteriaQuery<T> query, CriteriaBuilder builder,
			Root<R> root) {
		joinTable(filter.getJoinList(), root);
		select(filter, query, builder, root);
		where(filter, query, builder, root);
		orderBy(filter, query, builder, root);
		groupBy(filter, query, builder, root);
	}

	public static <T, R> void conditionHandle(CommonFilter filter, Subquery<T> query, CriteriaBuilder builder,
			Root<R> root) {
		joinTable(filter.getJoinList(), root);
		select(filter, query, builder, root);
		where(filter, query, builder, root);
		groupBy(filter, query, builder, root);
	}

	public static <T, R> void conditionHandle(CommonFilter filter, Pageable pageable, CriteriaQuery<T> query,
			CriteriaBuilder builder, Root<R> root) {
		joinTable(filter.getJoinList(), root);
		select(filter, query, builder, root);
		where(filter, query, builder, root);
		orderBy(filter, query, pageable, builder, root);
		groupBy(filter, query, builder, root);
	}

	public static <R> void countConditionHandle(CommonFilter filter, CriteriaQuery<Long> query, CriteriaBuilder builder,
			Root<R> root) {
		joinTable(filter.getJoinList(), root);
		countSelect(filter, query, builder, root);
		where(filter, query, builder, root);
	}

	public static <T> CriteriaQuery<T> select(CommonFilter commonFilter, CriteriaQuery<T> query,
			CriteriaBuilder criteriaBuilder, Path<?> path) {
		List<SelectCondition> element = commonFilter.getSelectors();
		if (!element.isEmpty()) {
			Expression<?>[] expressions = element.stream().map(x -> x.select(criteriaBuilder, path))
					.toArray(Expression<?>[]::new);
			query.multiselect(expressions);
		}
		return query;
	}

	@SuppressWarnings("unchecked")
	public static <T> Subquery<T> select(CommonFilter commonFilter, Subquery<T> query, CriteriaBuilder criteriaBuilder,
			Path<?> path) {
		List<SelectCondition> element = commonFilter.getSelectors();

		if (!element.isEmpty()) {
			Expression<T> expression = (Expression<T>) element.stream().map(x -> x.select(criteriaBuilder, path))
					.findFirst().orElse(null);
			if (expression == null)
				throw new JpaAmebaException("No expression can be used");
			query.select(expression);
		}
		return query;
	}

	public static <T> CriteriaQuery<Long> countSelect(CommonFilter commonFilter, CriteriaQuery<Long> query,
			CriteriaBuilder criteriaBuilder, Path<T> path) {
		if (commonFilter.getGroupingBy().size() > 0)
			query.select(criteriaBuilder.countDistinct(commonFilter.getGroupingBy().get(0).groupBy(path)));
		else if (commonFilter.getCountField() != null)
			query.select(criteriaBuilder.countDistinct(PathUtils.getPath(commonFilter.getCountField(), path)));
		else
			query.select(criteriaBuilder.countDistinct(path));
		return query;
	}

}
