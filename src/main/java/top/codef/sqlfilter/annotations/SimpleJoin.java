package top.codef.sqlfilter.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.persistence.criteria.JoinType;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SimpleJoin {

	JoinType joinType() default JoinType.INNER;

	String value() ;
}
