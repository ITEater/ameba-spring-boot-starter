package top.codef.sqlfilter.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import top.codef.sqlfilter.enums.FunctionSymbol;
import top.codef.sqlfilter.enums.HavingSimbol;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface HavingCondition {

	String value() default "";

	FunctionSymbol function();

	HavingSimbol condition() default HavingSimbol.EQ;
}
