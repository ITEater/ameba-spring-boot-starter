package top.codef.sqlfilter.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import top.codef.sqlfilter.FilterSymbol;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface WhereCondition {

	String value() default "";

	FilterSymbol condition() default FilterSymbol.EQ;

	
}
