package top.codef.sqlfilter.utils;

public class LikePattern {

	public static String rightLike(String val) {
		return val == null ? null : String.format("%%%s", val);
	}

	public static String leftLikie(String val) {
		return val == null ? null : String.format("%s%%", val);
	}

	public static String middleLike(String val) {
		return val == null ? null : String.format("%%%s%%", val);
	}

}
