package top.codef.sqlfilter;

public class JoinCommonFilter extends AbstractFilter<JoinCommonFilter> {

	protected final String joinField;

	protected boolean joinObjectOnly = false;

	public JoinCommonFilter(String joinField) {
		super();
		this.joinField = joinField;
	}

	public JoinCommonFilter onlyJoinObj() {
		joinObjectOnly = true;
		return this;
	}

	public String getJoinField() {
		return joinField;
	}

	public boolean isJoinObjectOnly() {
		return joinObjectOnly;
	}

	public void setJoinObjectOnly(boolean joinObjectOnly) {
		this.joinObjectOnly = joinObjectOnly;
	}

}
