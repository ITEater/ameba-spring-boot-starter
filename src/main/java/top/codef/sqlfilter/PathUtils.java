package top.codef.sqlfilter;

import org.apache.commons.lang3.StringUtils;

import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Path;
import top.codef.exceptions.JpaAmebaException;

public class PathUtils {

	public static Expression<?> getPath(String pahtName, Expression<?> path) {
		if (StringUtils.isBlank(pahtName))
			throw new JpaAmebaException("不存在的字段信息");
		for (String speratedField : pahtName.split("\\.")) {
			if (StringUtils.isBlank(speratedField))
				throw new JpaAmebaException("字段信息不正确");
			if (Path.class.isAssignableFrom(path.getClass())) {
				var thePath = (Path<?>) path;
				path = thePath.get(speratedField);
			}
		}
		return path;
	}

}
