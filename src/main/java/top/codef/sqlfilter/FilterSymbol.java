package top.codef.sqlfilter;

import java.util.Collection;
import java.util.List;

import top.codef.sqlfilter.functional.FieldConditionFilter;

public enum FilterSymbol {

	EQ("等于", (filter, field, val) -> {
		filter.eq(field, val);
	}),

	NEQ("不等于", (filter, field, val) -> {
		filter.neq(field, val);
	}),

	LIKE("近似", (filter, field, val) -> {

		if (val != null) {
			var like = String.format("%%%s%%", val);
			filter.like(field, like);
		}
	}),

	ISNULL("为null", (filter, field, val) -> {
		if (val instanceof Boolean)
			if ((boolean) val)
				filter.isNull(field);
	}),

	ISNOTNULL("不为null", (filter, field, val) -> {
		if (val instanceof Boolean)
			if ((boolean) val)
				filter.isNotNull(field);
	}),

	IN("在列表内", (filter, field, val) -> {
		if (val == null)
			return;
		if (Collection.class.isAssignableFrom(val.getClass())) {
			var col = (Collection<?>) val;
			filter.in(field, col);
		}
	}),

	NOTIN("不在列表内", (filter, field, val) -> {
		if (val == null)
			return;
		if (Collection.class.isAssignableFrom(val.getClass())) {
			var col = (Collection<?>) val;
			filter.notIn(field, col);
		}
	}),

	@SuppressWarnings({ "rawtypes", "unchecked" })
	LT("小于", (filter, field, val) -> {
		if (val == null)
			return;
		if (Comparable.class.isAssignableFrom(val.getClass())) {
			var col = (Comparable) val;
			filter.lt(field, col);
		}
	}),

	@SuppressWarnings({ "rawtypes", "unchecked" })
	LE("小于等于", (filter, field, val) -> {
		if (val == null)
			return;
		if (Comparable.class.isAssignableFrom(val.getClass())) {
			var col = (Comparable) val;
			filter.le(field, col);
		}

	}),

	@SuppressWarnings({ "rawtypes", "unchecked" })
	GT("大于", (filter, field, val) -> {
		if (val == null)
			return;
		if (Comparable.class.isAssignableFrom(val.getClass())) {
			var col = (Comparable) val;
			filter.gt(field, col);
		}
	}),

	@SuppressWarnings({ "rawtypes", "unchecked" })
	GE("大于等于", (filter, field, val) -> {
		if (val == null)
			return;
		if (Comparable.class.isAssignableFrom(val.getClass())) {
			var col = (Comparable) val;
			filter.ge(field, col);
		}
	}),

	@SuppressWarnings({ "unchecked", "rawtypes" })
	BETWEEN("两者之间", (filter, field, val) -> {
		if (val == null)
			return;
		if (val instanceof SectionFilter) {
			var section = (SectionFilter) val;
			filter.between(field, section.getBegin(), section.getEnd());
		} else if (val instanceof List && ((List) val).size() == 2) {
			List<? extends Comparable> list = (List<? extends Comparable>) val;
			filter.between(field, list.get(0), list.get(1));
		}
	});

	private final String explain;

	private final FieldConditionFilter conditionFilter;

	private FilterSymbol(String explain, FieldConditionFilter conditionFilter) {
		this.explain = explain;
		this.conditionFilter = conditionFilter;
	}

	public String getExplain() {
		return explain;
	}

	public FieldConditionFilter getConditionFilter() {
		return conditionFilter;
	}
}
