package top.codef.sqlfilter;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Subquery;
import top.codef.exceptions.JpaAmebaException;

public class SubCommonFilter<ROOT, TARGET> extends CommonFilter {

	protected CommonFilter parent;

	protected SubSymbol subSymbol = SubSymbol.ANY;

	protected Class<ROOT> rootClazz;

	protected Class<TARGET> targetClazz;

	protected String fkField;

	public SubCommonFilter(CommonFilter parent, Class<ROOT> rootClazz, Class<TARGET> targetClazz) {
		this.parent = parent;
		this.rootClazz = rootClazz;
		this.targetClazz = targetClazz;
	}

	public CommonFilter and() {
		return parent;
	}

	public SubCommonFilter<ROOT, TARGET> all() {
		subSymbol = SubSymbol.ALL;
		return this;
	}

	public SubCommonFilter<ROOT, TARGET> any() {
		subSymbol = SubSymbol.ANY;
		return this;
	}

	public SubCommonFilter<ROOT, TARGET> fkField(String field) {
		fkField = field;
		return this;
	}

	public String getFkField() {
		return fkField;
	}

	public void setFkField(String fkField) {
		this.fkField = fkField;
	}

	public CommonFilter getParent() {
		return parent;
	}

	public void setParent(CommonFilter parent) {
		this.parent = parent;
	}

	public SubSymbol getSubSymbol() {
		return subSymbol;
	}

	public void setSubSymbol(SubSymbol subSymbol) {
		this.subSymbol = subSymbol;
	}

	public Class<ROOT> getRootClazz() {
		return rootClazz;
	}

	public void setRootClazz(Class<ROOT> rootClazz) {
		this.rootClazz = rootClazz;
	}

	public Class<TARGET> getTargetClazz() {
		return targetClazz;
	}

	public void setTargetClazz(Class<TARGET> targetClazz) {
		this.targetClazz = targetClazz;
	}

	public <T> Expression<T> genSub(CriteriaBuilder builder, Subquery<T> subquery) {
		switch (subSymbol) {
		case ALL:
			return builder.all(subquery);
		case ANY:
			return builder.any(subquery);
		default:
			throw new JpaAmebaException("no subSymbol");
		}
	}

}
