package top.codef.sqlfilter;

import top.codef.sqlfilter.enums.SubSymbol;

public class SubCommonFilter<ROOT, TARGET> extends AbstractFilter<SubCommonFilter<ROOT, TARGET>> {

	protected SubSymbol subSymbol = SubSymbol.ANY;

	protected Class<ROOT> rootClazz;

	protected Class<TARGET> targetClazz;

	protected String fkField;

	public SubCommonFilter(Class<ROOT> rootClazz, Class<TARGET> targetClazz) {
		this.rootClazz = rootClazz;
		this.targetClazz = targetClazz;
	}

	public SubCommonFilter<ROOT, TARGET> all() {
		subSymbol = SubSymbol.ALL;
		return this;
	}

	public SubCommonFilter<ROOT, TARGET> any() {
		subSymbol = SubSymbol.ANY;
		return this;
	}

	public SubCommonFilter<ROOT, TARGET> fkField(String field) {
		fkField = field;
		return this;
	}

	public String getFkField() {
		return fkField;
	}

	public void setFkField(String fkField) {
		this.fkField = fkField;
	}

	public SubSymbol getSubSymbol() {
		return subSymbol;
	}

	public void setSubSymbol(SubSymbol subSymbol) {
		this.subSymbol = subSymbol;
	}

	public Class<ROOT> getRootClazz() {
		return rootClazz;
	}

	public void setRootClazz(Class<ROOT> rootClazz) {
		this.rootClazz = rootClazz;
	}

	public Class<TARGET> getTargetClazz() {
		return targetClazz;
	}

	public void setTargetClazz(Class<TARGET> targetClazz) {
		this.targetClazz = targetClazz;
	}
}
