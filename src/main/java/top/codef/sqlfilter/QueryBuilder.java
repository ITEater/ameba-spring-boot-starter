package top.codef.sqlfilter;

import top.codef.sqlfilter.typed.TypedCommonFilter;

public class QueryBuilder {

	public static CommonFilter createFilter() {
		return new CommonFilter();
	}

	public static <R, T> SubCommonFilter<R, T> subQuery(Class<R> root, Class<T> tar) {
		return new SubCommonFilter<R, T>(null, root, tar);
	}

	public static <T> TypedCommonFilter<T> typeFilter(Class<T> clazz) {
		return new TypedCommonFilter<>(clazz);
	}

}
