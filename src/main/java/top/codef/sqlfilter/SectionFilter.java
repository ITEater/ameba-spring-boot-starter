package top.codef.sqlfilter;

public interface SectionFilter<T extends Comparable<? super T>> {

	T getBegin();

	T getEnd();
}
