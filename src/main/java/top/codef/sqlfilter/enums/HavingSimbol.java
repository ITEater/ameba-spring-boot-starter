package top.codef.sqlfilter.enums;

import java.util.Collection;

import top.codef.sqlfilter.functional.FieldHavingConditionFilter;

public enum HavingSimbol {

	EQ((filter, condition, val) -> {
		filter.eq(condition, val);
	}),

	NEQ((filter, field, val) -> {
		filter.neq(field, val);
	}),

	LIKE((filter, field, val) -> {

		if (val != null) {
			var like = String.format("%%%s%%", val);
			filter.like(field, like);
		}
	}),

	ISNULL((filter, field, val) -> {
		if (val instanceof Boolean)
			if ((boolean) val)
				filter.isNull(field);
	}),

	ISNOTNULL((filter, field, val) -> {
		if (val instanceof Boolean)
			if ((boolean) val)
				filter.isNotNull(field);
	}),

	IN((filter, field, val) -> {
		if (val == null)
			return;
		if (Collection.class.isAssignableFrom(val.getClass())) {
			var col = (Collection<?>) val;
			filter.in(field, col);
		}
	}),

	NOTIN((filter, field, val) -> {
		if (val == null)
			return;
		if (Collection.class.isAssignableFrom(val.getClass())) {
			var col = (Collection<?>) val;
			filter.notIn(field, col);
		}
	}),

	@SuppressWarnings({ "rawtypes", "unchecked" })
	LT((filter, field, val) -> {
		if (val == null)
			return;
		if (Comparable.class.isAssignableFrom(val.getClass())) {
			var col = (Comparable) val;
			filter.lt(field, col);
		}
	}),

	@SuppressWarnings({ "rawtypes", "unchecked" })
	LE((filter, field, val) -> {
		if (val == null)
			return;
		if (Comparable.class.isAssignableFrom(val.getClass())) {
			var col = (Comparable) val;
			filter.le(field, col);
		}

	}),

	@SuppressWarnings({ "rawtypes", "unchecked" })
	GT((filter, field, val) -> {
		if (val == null)
			return;
		if (Comparable.class.isAssignableFrom(val.getClass())) {
			var col = (Comparable) val;
			filter.gt(field, col);
		}
	}),

	@SuppressWarnings({ "rawtypes", "unchecked" })
	GE((filter, field, val) -> {
		if (val == null)
			return;
		if (Comparable.class.isAssignableFrom(val.getClass())) {
			var col = (Comparable) val;
			filter.ge(field, col);
		}
	});

	private HavingSimbol(FieldHavingConditionFilter conditionFilter) {
		this.conditionFilter = conditionFilter;
	}

	private final FieldHavingConditionFilter conditionFilter;

	public FieldHavingConditionFilter getConditionFilter() {
		return conditionFilter;
	}

}
