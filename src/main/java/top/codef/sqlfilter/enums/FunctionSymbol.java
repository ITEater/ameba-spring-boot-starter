package top.codef.sqlfilter.enums;

import java.util.function.Function;
import static top.codef.sqlfilter.Selectors.*;

import top.codef.sqlfilter.functional.SelectCondition;

public enum FunctionSymbol {

	SELECT(field -> select(field)),

	AVG(field -> avg(field)),

	MAX(field -> max(field)),

	ABS(field -> abs(field)),

	SUM(field -> sum(field)),

	DISTINCT(field -> distinct(field)),

	DISTINCT_COUNT(field -> distinctCount(field));

	FunctionSymbol(Function<String, SelectCondition> select) {
		this.select = select;
	}

	private final Function<String, SelectCondition> select;

	public Function<String, SelectCondition> getSelect() {
		return select;
	}

}
