package top.codef.sqlfilter.queryobj;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import top.codef.sqlfilter.SectionFilter;

public class LocalDatetimeSection implements SectionFilter<LocalDateTime> {

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime begin;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime end;

	public LocalDatetimeSection() {
	}

	public LocalDatetimeSection(LocalDateTime begin, LocalDateTime end) {
		this.begin = begin;
		this.end = end;
	}

	public LocalDateTime getBegin() {
		return begin;
	}

	public void setBegin(LocalDateTime begin) {
		this.begin = begin;
	}

	public LocalDateTime getEnd() {
		return end;
	}

	public void setEnd(LocalDateTime end) {
		this.end = end;
	}

}
