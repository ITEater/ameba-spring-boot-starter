package top.codef.sqlfilter.queryobj;

import top.codef.enums.OrderEnum;

public class OrderByCondition {

	private String field;

	private OrderEnum order;

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public OrderEnum getOrder() {
		return order;
	}

	public void setOrder(OrderEnum order) {
		this.order = order;
	}

}
