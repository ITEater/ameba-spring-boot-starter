package top.codef.sqlfilter.queryobj;

import top.codef.sqlfilter.functional.SectionFilter;

public class BaseSection<T extends Comparable<T>> implements SectionFilter<T> {

	private T begin;

	private T end;

	public T getBegin() {
		return begin;
	}

	public void setBegin(T begin) {
		this.begin = begin;
	}

	public T getEnd() {
		return end;
	}

	public void setEnd(T end) {
		this.end = end;
	}

}
