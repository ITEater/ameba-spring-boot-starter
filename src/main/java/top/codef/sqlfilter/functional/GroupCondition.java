package top.codef.sqlfilter.functional;

import jakarta.persistence.criteria.Expression;

@FunctionalInterface
public interface GroupCondition extends FilterModel {

	Expression<?> groupBy(Expression<?> path);
}
