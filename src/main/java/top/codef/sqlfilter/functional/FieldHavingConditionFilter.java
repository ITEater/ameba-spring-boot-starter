package top.codef.sqlfilter.functional;

import top.codef.sqlfilter.HavingCommonFilter;

@FunctionalInterface
public interface FieldHavingConditionFilter {

	void filter(HavingCommonFilter commonFilter, SelectCondition condition, Object QueryVal);
}
