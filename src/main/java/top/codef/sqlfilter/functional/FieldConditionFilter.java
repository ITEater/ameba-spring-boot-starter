package top.codef.sqlfilter.functional;

import top.codef.sqlfilter.CommonFilter;

@FunctionalInterface
public interface FieldConditionFilter {

	void filter(CommonFilter commonFilter, String field, Object QueryVal);
}
