package top.codef.sqlfilter.functional;

import top.codef.sqlfilter.AbstractFilter;

@FunctionalInterface
public interface FieldConditionFilter {

	void filter(AbstractFilter<?> commonFilter, String field, Object QueryVal);
}
