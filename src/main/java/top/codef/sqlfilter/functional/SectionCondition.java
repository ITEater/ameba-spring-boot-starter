package top.codef.sqlfilter.functional;

import top.codef.sqlfilter.CommonFilter;

public interface SectionCondition {

	void filter(CommonFilter commonFilter, String field, Object start, Object end);
}
