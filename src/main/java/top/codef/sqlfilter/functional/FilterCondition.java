package top.codef.sqlfilter.functional;

import jakarta.persistence.criteria.CommonAbstractCriteria;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;

@FunctionalInterface
public interface FilterCondition extends FilterModel {

	public Predicate condition(CommonAbstractCriteria query, CriteriaBuilder builder, Expression<?> path);
}
