package top.codef.sqlfilter.functional;

import java.util.function.Supplier;

import top.codef.sqlfilter.CommonFilter;

public interface GlobalSupplier extends Supplier<CommonFilter> {

}
