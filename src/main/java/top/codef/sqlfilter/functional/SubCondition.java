package top.codef.sqlfilter.functional;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import top.codef.sqlfilter.CommonFilter;

@FunctionalInterface
public interface SubCondition {

	public Predicate condition(CriteriaBuilder builder, CriteriaQuery<?> query, CommonFilter filter);
}
