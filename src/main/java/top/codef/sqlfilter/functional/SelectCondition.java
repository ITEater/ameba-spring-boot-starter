package top.codef.sqlfilter.functional;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Expression;

@FunctionalInterface
public interface SelectCondition extends FilterModel {

	Expression<?> select(CriteriaBuilder builder, Expression<?> path);
}
