package top.codef.sqlfilter.functional;

public interface SectionFilter<T extends Comparable<? super T>> {

	T getBegin();

	T getEnd();
}
