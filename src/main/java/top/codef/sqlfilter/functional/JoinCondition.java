package top.codef.sqlfilter.functional;

import jakarta.persistence.criteria.CommonAbstractCriteria;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Root;

@FunctionalInterface
public interface JoinCondition extends FilterModel {

	Join<?, ?> join(CommonAbstractCriteria criteria, Root<?> root, CriteriaBuilder builder);
}
